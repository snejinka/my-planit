/*global require*/
const {
  STYLE_WATCH_PATH,
  STYLE_SRC_PATH,
  STYLE_GUIDE_SRC_PATH,
  JS_WATCH_PATH,
  HTML_WATCH_PATH,
  WORK_OUT_FOLDER,
  STYLE_GUIDE_FOLDER,
  PROD_FOLDER
} = require('./config.json');

//Gulp related dependencies
const gulp      = require('gulp'),
  gulpif        = require('gulp-if'),
  sass          = require('gulp-sass'),
  babel         = require('gulp-babel'),
  rename        = require('gulp-rename'),
  concat        = require('gulp-concat'),
  uglify        = require('gulp-uglify'),
  useref        = require('gulp-useref'),
  plumber       = require('gulp-plumber'),
  imagemin      = require('gulp-imagemin'),
  size          = require('gulp-filesize'),
  gulpStylelint = require('gulp-stylelint'),
  minifyCSS     = require('gulp-minify-css'),
  autoprefixer  = require('gulp-autoprefixer'),
  browserSync   = require('browser-sync').create(),
  reload   = browserSync.reload,
  shell = require('gulp-shell');

gulp.task('styleguid', shell.task('styledown public/styleguid/config.md > public/styleguid/styleguide.html'))

gulp.task('browser-sync', () =>{
  browserSync.init({
    open:false,
    injectChanges:true,
    server: [WORK_OUT_FOLDER,STYLE_GUIDE_FOLDER]
  })
})

gulp.task('js', () => {
  'use strict';

  const js = gulp.src([WORK_OUT_FOLDER + 'js/main.js'])
    .pipe(plumber())
});

gulp.task('vendors', () => {
  'use strict';

  const vendor_css = gulp.src([
    './node_modules/@fortawesome/fontawesome-free/css/all.css'
  ])
    .pipe(concat('vendors.css'))
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'));

  const vendor_js = gulp.src([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/@fortawesome/fontawesome-free/js/all.js',
    './node_modules/bootstrap/dist/js/bootstrap.js'
  ])
    .pipe(babel())
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'));
});

gulp.task('images', () => {
  return gulp.src(WORK_OUT_FOLDER + 'images/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest(PROD_FOLDER + 'images'))
});

gulp.task('fonts', () => {
  return gulp.src(WORK_OUT_FOLDER + 'fonts/*.*')
    .pipe(gulp.dest(PROD_FOLDER + 'fonts'))
});

gulp.task('lint-css', () => {
  return gulp.src('src/**/*.scss')
    .pipe(gulpStylelint({
      reporters: [
        { formatter: 'string', console: true }
      ],
      debug: true
    }));
});

gulp.task('scss-to-css', ['lint-css'], () => {
  'use strict';

  gulp.src(STYLE_SRC_PATH)
    .pipe(sass({
      errorLogToConsole: true,
      outputStyle: 'compressed'
    }))
    .on('error', console.error.bind(console))
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'))
    .pipe(browserSync.stream());

  gulp.src(STYLE_GUIDE_SRC_PATH)
    .pipe(sass({
      errorLogToConsole: true,
      outputStyle: 'compressed'
    }))
    .on('error', console.error.bind(console))
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(STYLE_GUIDE_FOLDER))
    .pipe(browserSync.stream());

});

gulp.task('default', ['scss-to-css','js']);

gulp.task('watch',['default', 'browser-sync'], () =>{
  gulp.watch( STYLE_WATCH_PATH, ['scss-to-css']);
  gulp.watch( JS_WATCH_PATH, ['js', reload]);
  gulp.watch( HTML_WATCH_PATH, reload);
  gulp.watch(STYLE_GUIDE_FOLDER + '**/*.md', ['styleguid']);
  gulp.watch(STYLE_GUIDE_FOLDER + '**/*.css', ['styleguid']);
  gulp.watch(STYLE_GUIDE_FOLDER + '*.html',() => setTimeout(reload,1500));

});

gulp.task('bundle', ['scss-to-css', 'vendors', 'images', 'fonts'], () => {
  return gulp.src([WORK_OUT_FOLDER + '*.html'])
    .pipe(useref())
    .pipe(gulpif('main.js', uglify()))
    .pipe(gulpif('*.css', minifyCSS()))
    .pipe(gulp.dest(PROD_FOLDER))
    .pipe(size());
});
