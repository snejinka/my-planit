`Colors`
--------------
    @example 
    .row
        .col
            p Main colors
            span.color-box.main-color
            span.color-box.main-color
            span.color-box.main-color
        
        .col  
            p Muted colors 
            span.color-box.muted-color
            span.color-box.muted-color
            span.color-box.muted-color
        .col  
            p Dark colors 
            span.color-box.dark-color
            span.color-box.dark-color
            span.color-box.dark-color

    


 
`Typography`
--------------
##On the site used the font Roboto from https://fonts.google.com
    @example
   
    .row    
        .col-4
            h1 Header H1
                span.content-text-small Roboto 48px
            h2 Header H2
                span.content-text-small Roboto 36px
            h3 Header H3
                span.content-text-small Roboto 28px
            h4 Header H4
                span.content-text-small Roboto 24px
            h5 Header H5
                span.content-text-small Roboto 20px
            h6 Header H6
                span.content-text-small Roboto 16px
        .col-4
            p.content-text
                a.link-primary(href='#') Link primary
                span
                    | Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad architecto laborum neque quae quia ratione repellendus sit tempora tempore voluptates. Ad aperiam est ex fuga laborum nemo praesentium repellat veniam.
                span
                    | Accusantium aliquid amet corporis eos et illo impedit iste, labore laudantium, maiores nam quos vitae voluptas. Alias aliquid aut enim ipsa ipsam libero minima, nemo nobis praesentium quibusdam soluta ullam?
        .col-4
            p.content-text-small
                a.link-primary(href='#') Link primary
                span
                    | Accusamus aliquid architecto beatae corporis deserunt eum expedita impedit iusto laboriosam, laborum modi nesciunt officia omnis optio porro quo quos, recusandae sed tenetur unde vel, vero voluptate. Ex, laboriosam placeat!
                span
                    | A alias asperiores aspernatur at aut cumque debitis dignissimos doloremque ea et ipsum iusto minus nam nisi nostrum nulla odit perferendis perspiciatis placeat, qui, quis ratione recusandae ut voluptate voluptatibus.
                    
            .row
                .col
                    p.title.text-bold Bold text
                    p.title.text-normal Normal text 
                    p.title.text-light-weight Light text 

`Buttons`
--------------
    @example
    .row    
        .col-2   
            button.btn.btn-primary.btn-lg Button primary
            button.btn.btn-primary Button primary
            button.btn.btn-primary.btn-sm Button primary
        .col-2
            button.btn.btn-outline-primary.btn-lg Button outline
            button.btn.btn-outline-primary Button outline
            button.btn.btn-outline-primary.btn-sm Button outline  
        .col-2
            button.btn.btn-primary.btn-rounded.btn-lg Button rounded
            button.btn.btn-primary.btn-rounded Button rounded
            button.btn.btn-primary.btn-rounded.btn-sm Button rounded 
   
          
          
          
`Table`
--------------
    @example
    #pricing-section.pricing-section
      .container
        h3.title.text-center.text-bold.d-md-none.d-lg-none.d-xs-block.d-sm-block myPlanit Pricing Plans
        .btn-group.plan-switch-container(role='group', aria-label='Basic example')
          button.btn.btn-secondary.plan-switcher-btn.plan-switcher-btn-left.plan-switcher-btn-active(type='button') Basic Plan
          button.btn.btn-secondary.plan-switcher-btn.plan-switcher-btn-right(type='button') Professional Plan
        .row
          .col
            .pricing-wrap
              .table-responsive-md.table-fixed-left-column
                table.table.table-hover(width='100%')
                  colgroup
                    col
                    col
                    col
                  thead
                    tr.table-title.d-none.d-sm-none.d-md-table-row.d-lg-table-row
                      td
                      td(colspan='2')
                        h3.title.text-center.text-bold myPlanit Pricing Plans
                    tr.plan-header
                      th(scope='col')
                        span.plan-header-title Sign up now for a 30-day free trial!
                      th.plan-header-col(scope='col')
                        .plan-header-item
                          span.plan-name Basic
                          span.plan-price
                            sup $
                            | 9.99/mo
                          span.plan-price.plan-price-small
                            sup $
                            | 8.33/mo if billed annually
                          span.plan-sale.hidden-mobile Save 17% by paying annually
                          span.plan-sale.visible-mobile (a 17% savings)
                          button.btn.btn-primary Start Free Trial
                      th.plan-header-col(scope='col')
                        .plan-header-item
                          span.plan-name Professional
                          span.plan-price
                            sup $
                            | 24.99/mo
                          span.plan-price.plan-price-small
                            sup $
                            | 20.83/mo if billed annually
                          span.plan-sale.hidden-mobile Save 17% by paying annually
                          span.plan-sale.visible-mobile (a 17% savings)
                          button.btn.btn-primary Start Free Trial
                  tbody
                    tr.plan-row
                      td.plan-feature Customized client-specific searches
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Track favorite properties by client
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Client timeline with property history
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Smart reminders to complete showing notes
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Automated mileage tracking connected to MLS listing data
                      td Up to 40 drives
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Automated mileage expense reporting
                      td Up to 40 drives
                      td Unlimited
                    tr.plan-row
                      td.plan-feature “Real-Time”* MLS data, including Days-On-Market and sold data
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature “Real-time”* client-personalized MLS hot sheets with auto-alerts
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Auto-track MLS properties visited
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature
                        | Comparable properties, real-time for any listing, including list-to-sold ratio
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Intuitive search for client activity and notes
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Listings search with automatic client match
                      td Unlimited
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Property notes
                      td Unlimited
                      td Unlimited
                    tr.plan-row
                      td.plan-feature MLS listing data auto-synced to client appointments
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Enhanced video training and tutorials
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                  tfoot
                    tr.plan-row.plan-row-action
                      td.plan-td-note
                        p.content-text-small
                          | *Real-time updates are subject to MLS data availability. Some MLS areas may be 2-4
                          | hours delayed.
                      td.plan-td-action
                        button.btn.btn-primary Start Free Trial
                      td.plan-td-action
                        button.btn.btn-primary Start Free Trial
    #pricing-section.pricing-section
        h3.title.text-center.text-bold.d-md-none.d-lg-none.d-xs-block.d-sm-block myPlanit Pricing Plans
        .btn-group.plan-switch-container(role='group', aria-label='Basic example')
          button.btn.btn-secondary.plan-switcher-btn.plan-switcher-btn-left.plan-switcher-btn-active(type='button') Basic Plan
          button.btn.btn-secondary.plan-switcher-btn.plan-switcher-btn-right(type='button') Professional Plan
        .row
          .col
            .pricing-wrap.pricing-wrap-margins
              .table-responsive-md.table-fixed-left-column.table-separated
                table.table.table-hover(width='100%')
                  colgroup
                    col
                    col
                    col
                  thead
                    tr.table-title.d-none.d-sm-none.d-md-table-row.d-lg-table-row
                      td
                      td(colspan='2')
                        h3.title.text-center.text-bold myPlanit Pricing Plans
                    tr.plan-header
                      th(scope='col')
                      th.plan-header-col.plan-header-col-separated(scope='col')
                        span.plan-name Basic
                        .plan-header-container
                          .plan-header-item
                            span.plan-name-small Monthly billing
                            span.plan-price
                              sup $
                              | 9.99/mo
                            span.plan-current-label Current plan
                            button.btn.btn-secondary Change Add-Ons
                          .plan-header-item
                            span.plan-name-small Annual billing
                            span.plan-price
                              sup $
                              | 8.33/mo
                            span.plan-sale 17% savings
                            button.btn.btn-primary Select this plan
                      th.plan-header-col.plan-header-col-separated(scope='col')
                        span.plan-name Professional
                        .plan-header-container
                          .plan-header-item
                            span.plan-name-small Monthly billing
                            span.plan-price.plan-price-offset
                              sup $
                              | 24.99/mo
                            button.btn.btn-primary Select this plan
                          .plan-header-item
                            span.plan-name-small Annual billing
                            span.plan-price
                              sup $
                              | 20.83/mo
                            span.plan-sale 17% savings
                            button.btn.btn-primary Select this plan
                  tbody
                    tr.plan-row
                      td.plan-feature Customized client-specific searches
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Track favorite properties by client
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Client timeline with property history
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Smart reminders to complete showing notes
                      td Up to 5 clients
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Automated mileage tracking connected to MLS listing data
                      td Up to 40 drives
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Automated mileage expense reporting
                      td Up to 40 drives
                      td Unlimited
                    tr.plan-row
                      td.plan-feature “Real-Time”* MLS data, including Days-On-Market and sold data
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature “Real-time”* client-personalized MLS hot sheets with auto-alerts
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Auto-track MLS properties visited
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature
                        | Comparable properties, real-time for any listing, including list-to-sold ratio
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Intuitive search for client activity and notes
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Listings search with automatic client match
                      td Unlimited
                      td Unlimited
                    tr.plan-row
                      td.plan-feature Property notes
                      td Unlimited
                      td Unlimited
                    tr.plan-row
                      td.plan-feature MLS listing data auto-synced to client appointments
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                    tr.plan-row
                      td.plan-feature Enhanced video training and tutorials
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                      td
                        i.fas.fa-check-circle.plan-yes-icon
                  tfoot
                    tr.plan-row.plan-row-action
                      td.plan-td-note
                        p.content-text-small
                          | *Real-time updates are subject to MLS data availability. Some MLS areas may be 2-4
                          | hours delayed.
                      td.plan-td-action.plan-td-action-separated
                        .plan-td-action-container
                          .plan-td-action-item
                            span.plan-name-small Monthly billing
                            button.btn.btn-secondary Change Add-Ons
                          .plan-td-action-item
                            span.plan-name-small Annual billing
                            button.btn.btn-primary Select this plan
                      td.plan-td-action.plan-td-action-separated
                        .plan-td-action-container
                          .plan-td-action-item
                            span.plan-name-small Monthly billing
                            button.btn.btn-primary Select this plan
                          .plan-td-action-item
                            span.plan-name-small Annual billing
                            button.btn.btn-primary Select this plan



`Waves animated`
--------------
    @example
    .row    
        .col-7
            .wave-container
                .waveWrapper.waveAnimation
                  .waveWrapperInner.bg-wave-one
                    .wave.wave-one
                  .waveWrapperInner.bg-wave-two
                    .wave.wave-two
                  .waveWrapperInner.bg-wave-three
                    .wave.wave-three
                  .waveWrapperInner.bg-wave-four
                    .wave.wave-four
        .col-5
            h3 CSS code for waves
            p Each wave is animated separately.
            p Here is an example of using animation for one of the waves.
            code
              | background-image: url(../images/blue-line-thicker.png);
              br
              | background-size: 50% 100%;
              br
              | animation: move_wave 20s linear infinite;
            p Animation key frames
            code
              | @keyframes move_wave {
              br
              | 0% {
              br
              | transform: translateX(0) translateZ(0) scaleY(1);
              br
              | }
              br
              | 50% {
              br
              | transform: translateX(-25%) translateZ(0) scaleY(0.55);
              br
              | }
              br
              | 100% {
              br
              | transform: translateX(-50%) translateZ(0) scaleY(1);
              br
              | }
              br
              | }
            
          
          
`Footer`
--------------
    @example
    .footer
      .container
        .row.label-list.text-center
          .col-12.col-md-4.col-md-0.offset-0
            img.label-list-item(src='../images/fwdInnovation.png')
          .col-12.col-md-4
            img.label-list-item(src='../images/inmanStars.png')
          .col-12.col-md-4
            img.label-list-item(src='../images/inmanInnovationAwardsAndText.png')
        .row.footer-list
          .col-12.col-sm-4.col-md-3.col-lg-2.offset-sm-0.offset-md-1.offset-lg-3.offset-0.text-center
            a.footer-list-link(href='#') Terms of Service
          .col-12.col-sm-4.col-md-3.col-lg-2.text-center
            a.footer-list-link(href='#') Privacy Policy
          .col-12.col-sm-4.col-md-4.col-lg-2.text-center
            a.footer-list-link(href='#') Learn More About myPlanit
        .row
          .col
            p.copyright myPlanit™ Inc. © 2018



# Styleguide options   

### Head
    link(rel='stylesheet' href='https://cdn.rawgit.com/styledown/styledown/v1.0.2/data/styledown.css')
    link(rel='stylesheet', href='./css/vendors.css')
    link(rel="stylesheet" href="../css/style.css")
    link(rel="stylesheet" href="../css/combined.css")
    link(rel="stylesheet" href="styleguid.css") 
    script(src='https://cdn.rawgit.com/styledown/styledown/v1.0.2/data/styledown.js')

### Body
    .container
        .row
            .col-2
                img.logo(src='../images/logo.png')
            .col-4.offset-2
                h1 myPlanit Styleguides
                
        div#styleguides(sg-content) 
    
    