$(function () {
  let responsiveTable = $(".table-responsive-md");
  responsiveTable.scroll(()=> {
    let scrollLeft = responsiveTable.scrollLeft();
    let maxScrollLeft = responsiveTable.prop("scrollWidth") - responsiveTable.prop("clientWidth");
    if(scrollLeft < (maxScrollLeft / 2)){
      $('.plan-switcher-btn-right').removeClass('plan-switcher-btn-active');
      $('.plan-switcher-btn-left').addClass('plan-switcher-btn-active');
    }else {
      $('.plan-switcher-btn-left').removeClass('plan-switcher-btn-active');
      $('.plan-switcher-btn-right').addClass('plan-switcher-btn-active');
    }
  })
  $('.plan-switcher-btn-left').on('click', (el) => {
    responsiveTable.scrollLeft(0);
    $('.plan-switcher-btn-right').removeClass('plan-switcher-btn-active');
    $('.plan-switcher-btn-left').addClass('plan-switcher-btn-active');
  })
  $('.plan-switcher-btn-right').on('click', (el) => {
    let maxScrollLeft = responsiveTable.prop("scrollWidth") - responsiveTable.prop("clientWidth");
    responsiveTable.scrollLeft(maxScrollLeft);
    $('.plan-switcher-btn-left').removeClass('plan-switcher-btn-active');
    $('.plan-switcher-btn-right').addClass('plan-switcher-btn-active');
  })
});